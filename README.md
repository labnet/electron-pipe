# electron-pipe

promise-based IPC for electron

## Install

    npm install --save bitbucket:labnet/electron-pipe#v0.2.0

## Example

### Sender (if renderer process)

    pipe.send('late-uppercase-name', 'tim')
        .then((name) => {
            console.log('Uppercase name: ', name);
        })
        .catch((err) => {
            console.log('Error ', err);
        });

### Sender (if main process)

    pipe.send(mainWindow.webContents, 'late-uppercase-name', 'tim')
        .then((name) => {
            console.log('Uppercase name: ', name);
        })
        .catch((err) => {
            console.log('Error ', err);
        });

### Receiver

    pipe.on('late-uppercase-name', (name) => {
        return Promise.delay(1000, name.toUpperCase());
    });
